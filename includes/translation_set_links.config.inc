<?php

// Need to use i18n_language_list
// Would like to move this to a vertical tab
foreach(language_list() as $langcode => $langauge) {
  $options[$langcode] = $langauge->name;
}

$text_options = array(
  'langcode' => 'Language Code',
  'langname' => 'Language Name',
  'title' => 'Page Title',
);

$form['settings'] = array(
  '#type' => 'fieldset',
  '#title' => t('Translation Set Links'),
  '#group' => 'visibility',
  '#weight' => -20,
);

$form['settings']['translation_set_links_menu_style'] = array(
  '#type' => 'select',
  '#title' => t('Menu Style'),
  '#options' => array(
    'select' => 'Select Menu',
    'fancy' => 'Fancy Fly Out',
    'ol' => 'Ordered List',
    'ul' => 'Unordered List',
    'span' => 'span',
    'div' => 'div',
  ),
  '#default_value' => variable_get('translation_set_links_menu_style', 'select'),
);

$form['settings']['translation_set_use_globe_icon'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use globe icon instead of block title'),
    '#return_value' => 1,
    '#default_value' => variable_get('translation_set_use_globe_icon', 0),
    '#states' => array(
      'visible' => array(
        ':input[name="translation_set_links_menu_style"]' => array('value' => 'fancy'),
      ),
    ),
  );

// If language icons module is enabled, give the admin more 
// options to display using languageicons_icon theme
if(module_exists('languageicons')) {
  $form['settings']['translation_set_links_show_icon'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Flag Icon'),
    '#return_value' => 1,
    '#default_value' => variable_get('translation_set_links_show_icon', 0),
    '#states' => array(
      'invisible' => array(
        ':input[name="translation_set_links_menu_style"]' => array('value' => 'select'),
      ),
    ),
  );

  $form['settings']['translation_set_links_text_before_icon'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check this box to display text before the flag icon'),
    '#return_value' => 1,
    '#default_value' => variable_get('translation_set_links_text_before_icon', 0),
    '#states' => array(
      'visible' => array(
        ':input[name="translation_set_links_show_icon"]' => array('checked' => TRUE),
      ),
    ),
  );

  $text_options += array('none' => 'Icon Only');
} else {
  $form['settings']['translation_set_links_show_icon'] = array(
    '#type' => 'markup',
    '#markup' => t('Install <a href="https://drupal.org/project/languageicons" target="_blank" title="Visit Drupal.org to download Language icons ">Language icons</a> module to enable icon feature</a>'),
  );
}

$form['settings']['translation_set_links_text'] = array(
  '#type' => 'select',
  '#title' => t('Text Display'),
  '#options' => $text_options,
  '#default_value' => variable_get('translation_set_links_text', 'langname'),
);

$form['settings']['translation_set_links_allowed_langs'] = array(
  '#type' => 'checkboxes',
  '#title' => t('Show Links for These Languages'),
  '#description' => 'If no language is selected, all available languages will display if content exists.',
  '#options' => $options,
  '#default_value' => variable_get('translation_set_links_allowed_langs', array()),
  '#weight' => 100,
);
