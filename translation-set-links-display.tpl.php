<?php

/**
 * @file
 * Display links to translation set for current node (page)
 *
 * Available variables:
 * - $links: Links to the available translation set.
 */
?>
<div class="translation-set-links-display">
<?php print $links; ?>
</div>
