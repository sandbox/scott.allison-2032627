
(function($) {
  Drupal.behaviors.translation_set_links_jump_menu = {
  	attach: function(context) {
  	  $('.translation-set-links-display select').change(function() {
  	  	if($(this).val().length) {
  			 location.href = $(this).val();
  		  }
      });
    }
  }

  Drupal.behaviors.flyoutDisplay = {
    attach: function (context, settings) {
      $('.translation-set-links-title').click(Drupal.flyoutShowLanguages);
      $('body').click(function(event) {
        var isVisible = $('#block-translation_set_links-translation_set_links').find('.translation-set-links-fancy-flyout').hasClass("visible");
        if(!$(event.target).closest('.translation-set-links-fancy-flyout, .translation-set-links-display, .translation-set-links-title').length && isVisible) {
          $('.translation-set-links-fancy-flyout').hide().removeClass('visible');
        }
      });
    }
  };

  Drupal.flyoutShowLanguages = function (context, params) {
    var $flyout = $('#block-translation_set_links-translation_set_links').find('.translation-set-links-fancy-flyout');
    
    if($flyout.hasClass('visible')) {
      $flyout.hide().removeClass('visible');
    } else {
      $flyout.show().addClass('visible');
    }
  }

})(jQuery);
